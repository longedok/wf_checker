Extracts javascript code from a json file and checks it for syntax errors. Requires acorn (https://github.com/ternjs/acorn) as a dependency.


Installation:
    npm install -g acorn (might require sudo on unix-like systems)


Example of usage:
    python wf_checker.py example.json

